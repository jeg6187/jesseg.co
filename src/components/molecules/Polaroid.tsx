import React, { useState } from 'react';
import styled from 'styled-components';
import { useSprings, animated, interpolate } from 'react-spring';

const cards = [
  'https://upload.wikimedia.org/wikipedia/en/f/f5/RWS_Tarot_08_Strength.jpg',
  'https://upload.wikimedia.org/wikipedia/en/5/53/RWS_Tarot_16_Tower.jpg',
  'https://upload.wikimedia.org/wikipedia/en/9/9b/RWS_Tarot_07_Chariot.jpg',
  'https://upload.wikimedia.org/wikipedia/en/d/db/RWS_Tarot_06_Lovers.jpg',
  'https://upload.wikimedia.org/wikipedia/en/thumb/8/88/RWS_Tarot_02_High_Priestess.jpg/690px-RWS_Tarot_02_High_Priestess.jpg',
  'https://upload.wikimedia.org/wikipedia/en/d/de/RWS_Tarot_01_Magician.jpg',
];

const to = (i: any) => ({
  x: 0,
  y: i * -4,
  scale: 1,
  rot: -10 + Math.random() * 20,
  delay: i * 100,
});

const from = (i: any) => ({ x: 0, rot: 0, scale: 1.5, y: -1000 });

const trans = (rotation: any, scale: any) =>
  `perspective(1500px) rotateX(30deg) rotateY(${
    rotation / 10
  }deg) rotateZ(${rotation}deg) scale(${scale})`;

const Component = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 30rem;
  height: 30rem;
  position: relative;
  border: 1px solid black;
`;

const Container = styled(animated.div)`
  position: absolute;
  width: min-content;
  height: min-content;
  will-change: transform;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Picture = styled(animated.div)`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  background: black;
  background-size: auto 100%;
  background-repeat: no-repeat;
  background-position: center center;
  width: 20rem;
  height: 20rem;
  will-change: transform;
  border: 1em solid white;
  border-bottom: 0px;
  border-radius: 0.25rem;
  box-shadow: ${(props) => props.theme.raised};
  cursor: grab;

  &:active {
    cursor: grabbing;
  }
`;

const Label = styled.div`
  text-align: center;
  font-family: ${(props) => props.theme.font.marker};
  color: black;
  font-size: 3rem;
  background-color: white;
  user-select: none;
`;

const Polaroid = (): JSX.Element => {
  const [props, set] = useSprings(cards.length, (i) => ({
    ...to(i),
    from: from(i),
  }));

  return (
    <Component>
      {props.map(({ x, y, rot, scale }, i) => (
        <Container
          key={i}
          style={{
            transform: interpolate(
              [x, y],
              (x, y) => `translate3d(${x}px,${y}px,0)`,
            ),
          }}
        >
          <Picture
            style={{
              transform: interpolate([rot, scale], trans),
              backgroundImage: `url(${cards[i]})`,
            }}
          >
            <Label>Test</Label>
          </Picture>
        </Container>
      ))}
    </Component>
  );
};

export default Polaroid;
