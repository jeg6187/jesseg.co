import React from 'react';
import styled from 'styled-components';

const Component = styled.div`
  display: flex;
  flex-wrap: wrap;
  max-width: max-content;
  color: ${(props) => props.theme.color.primary[1]};
  font-family: ${(props) => props.theme.font.serif};

  font-size: 4rem;

  ${(props) => props.theme.media.phone} {
    font-size: 2.1rem;
  }
`;

const H1 = styled.h1``;

const H2 = styled.h2``;

const H3 = styled.h3``;

const H4 = styled.h4``;

const H5 = styled.h5``;

const H6 = styled.h6``;

type TitleProps = {
  id?: string;
  className?: string;
  style?: any;
  children: React.React.ReactNode;
  load?: boolean;
  animate?: boolean;
};

const Title = ({ id, className, style, children }: TitleProps): JSX.Element => {
  return (
    <Component id={id} className={`title ${className}`} style={style}>
      {children}
    </Component>
  );
};

export default Title;
