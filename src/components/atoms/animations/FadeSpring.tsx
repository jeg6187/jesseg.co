import React from 'react';
import { useSpring, animated } from 'react-spring';
import styled from 'styled-components';

const Component = styled(animated.div)``;

export type FadeSpringProps = {
  children: React.ReactNode;
  style?: any;
  config?: any;
  delay?: number;
  animate?: boolean;
  swipe?: 'left' | 'right' | 'top' | 'bottom';
};

const FadeSpring = ({
  children,
  style,
  config,
  delay,
  animate = true,
  swipe,
}: FadeSpringProps): JSX.Element => {
  // ? determines the swipe direction for the component
  const swipeAnimation = { from: '', to: '' };
  const updateTransform = (from: string, to: string) =>
    Object.assign(swipeAnimation, { from, to });
  switch (swipe) {
    case 'left':
      updateTransform(`translateX(-100%)`, `translateX(0%)`);
      break;
    case 'right':
      updateTransform(`translateX(100%)`, `translateX(0%)`);
      break;
    case 'top':
      updateTransform(`translateY(-100%)`, `translateY(0%)`);
      break;
    case 'bottom':
      updateTransform(`translateY(100%)`, `translateY(0%)`);
      break;
  }

  const animation = useSpring({
    config: Object.assign({}, config),
    opacity: animate ? 1 : 0,
    transform: animate ? swipeAnimation.to : swipeAnimation.from,
    from: { opacity: 0, transform: swipeAnimation.from },
    delay,
  });

  return <Component style={{ ...animation, ...style }}>{children}</Component>;
};

export default FadeSpring;
