import React from 'react';
import styled from 'styled-components';

const Component = styled.div`
  display: flex;
  flex-direction: column;
  background: ${(props) => props.theme.color.background[2]};
  padding: 1rem;
  border-radius: 1rem;

  @media print {
    padding: 0.5rem;
    border: 2px solid black;
    background: transparent;
  }
`;

export type SectionProps = {
  className?: string;
  children?: React.ReactNode;
};

const Section = (props: SectionProps): JSX.Element => {
  return <Component {...props} />;
};

export default Section;
