import React from 'react';
import { useState, useEffect, useCallback } from 'react';
import styled from 'styled-components';

import { useHistory } from 'react-router-dom';
import { animated, useSpring } from 'react-spring';

import MenuButton from 'atoms/MenuButton';

const Component = styled(animated.div)<{ height: string }>`
  position: sticky;
  display: flex;
  flex: 1;
  justify-content: center;
  width: 100%;
  height: 100%;
  max-height: ${(props) => props.height};
  min-height: ${(props) => props.height};
  z-index: 100;
  top: 0;
  background: ${(props) => props.theme.color.background[1]};
  box-shadow: ${(props) => props.theme.raised};
`;

const MainMenuSubtainer = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  max-width: max-content;
  ${(props) => props.theme.media.phone} {
    max-width: 100%;
  }
`;

const MenuIndicator = styled(animated.div)`
  position: absolute;
  background: ${(props) => props.theme.color.primary[3]};
  height: 0.1rem;
  margin: 0.25rem;
  border-radius: 5px;
  left: 0;
  bottom: 0;
`;

// * add Menu entires here
const mainMenuItems = [
  { path: '/', label: 'Home', iconClass: 'fas fa-home' },
  { path: '/about', label: 'About', iconClass: 'fas fa-user-alt' },
  // { path: '/projects', label: 'Projects', iconClass: 'fas fa-code' },
  { path: '/resume', label: 'Resume', iconClass: 'fas fa-scroll' },
  // {
  //   path: '/contact',
  //   label: 'Contact',
  //   iconClass: 'fas fa-envelope-open-text',
  // },
];

// todo: refactor to use the 'useHistory' api from react-router

const MenuBar: React.FunctionComponent = () => {
  const history = useHistory();
  const menuBarHeight = '3em';

  const getMenuIndex = useCallback(
    (path: string) => mainMenuItems.map((item) => item.path).indexOf(path),
    [],
  );

  const [menuIndex, setMenuIndex] = useState(
    getMenuIndex(history.location.pathname),
  );

  useEffect(() => {
    history.listen((location) => {
      setMenuIndex(getMenuIndex(location.pathname));
    });
  }, [history, getMenuIndex, setMenuIndex]);

  const menuIndicatorSpring = useSpring({
    opacity: 1,
    marginLeft: `calc(${(100 / mainMenuItems.length) * menuIndex}%)`,
    from: {
      opacity: 0,
      width: `calc(${100 / mainMenuItems.length}%)`,
      marginLeft: `calc(${(100 / mainMenuItems.length) * menuIndex}%)`,
    },
  });

  return (
    <Component height={menuBarHeight}>
      <MainMenuSubtainer>
        <MenuIndicator style={menuIndicatorSpring} />
        {mainMenuItems.map((menuItem, index) => (
          <MenuButton key={index} {...menuItem} />
        ))}
      </MainMenuSubtainer>
    </Component>
  );
};

export default MenuBar;
