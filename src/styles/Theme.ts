declare module 'styled-components' {
  type Theme = typeof Theme;
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  export interface DefaultTheme extends Theme {}
}

const maxMedia = (maxWidth: number) => `@media (max-width: ${maxWidth}px)`;
const minMedia = (minWidth: number) => `@media (min-width: ${minWidth}px)`;

const Theme = {
  font: {
    serif: "'Rokkitt', serif",
    sanSerif: "'Ubuntu', sans-serif",
    marker: "'Permanent Marker', cursive",
  },
  color: {
    text: '#cccccc',
    primary: {
      1: '#e79055',
      2: '#df270f',
      3: '#35d2c9',
    },
    background: {
      1: '#383734',
      2: '#353431',
    },
  },
  raised: '1px 0px 5px black',
  media: {
    desktop: minMedia(922),
    ultrawide: minMedia(2343),
    tablet: maxMedia(768),
    phone: maxMedia(576),
  },
};

export default Theme;
