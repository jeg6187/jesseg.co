import React from 'react';

import PageSegment from 'atoms/PageSegment';
import Paragraph from 'atoms/elements/Paragraph';
import Title from 'atoms/elements/Title';
// import Polaroid from 'molecules/Polaroid';

const AboutPage = (): JSX.Element => {
  return (
    <>
      <PageSegment>
        <Title animate>Who am I?</Title>
        <Paragraph>
          I'm a young software engineer who grew up in a small town in Utah! I'm
          an avid biker + longboarder, casual hiker, and creative who loves to
          express himself through code and the internet!
        </Paragraph>
        {/* <Polaroid /> */}
      </PageSegment>

      <PageSegment>
        <Title>How'd I get my start into Software Engineering?</Title>
        <Paragraph>
          It started as a child with my curiosity for the mysteries of the
          computer and the internet. I would always love to tinker with the
          computer and the various programs on it. One of those programs was
          Minecraft; I loved going into the code and modifying it with silly
          little tweaks. This gave me such a rush of joy and from them on
          something just clicked in my head. I knew that I needed to do more
          with the computer in my life.
        </Paragraph>
        <Paragraph>
          I became infatuated with the internet + coding at a young age and
          started messing around with html, css, and javascript early in my
          highschool years. Fascinated by the possibilities, my childhood
          friend, Chad Malmrose, pushed me to new heights. We joined a basic
          programming course half way through sophomore year, learning various
          languages, we caught up with the class and managed to finish at the
          top with our peers.
        </Paragraph>
        <Paragraph>
          Junior year, we started a new chapter in our lives, learning more and
          more with what we could do with this new found tool that was coding.
          Our mentor, Sam Haas, sparked our interests in programming and we were
          off! It was that year I realized that this is what I wanted to do for
          the rest of my life! building cool stuff with the new found
          programming languages we had learned!
        </Paragraph>
        <Paragraph>
          By Senior year, our school offered a school funded program called
          C.A.P.S. to work with small + local business to help students explore
          and learn about various industries/carriers in a project based
          environment. I joined this program and became a team lead for various
          coding projects, teaching students some of the basics of programming,
          managing timelines, and delivering small website to various local
          businesses.
        </Paragraph>
        <Paragraph>
          By the end of the year, I had gained valuable knowledge and had landed
          a internship at a small company in Park city, Utah for the Summer.
          Although short and humbling, I realized there was still much I needed
          to learn, so I applied for a course at a technical college in Lehi
          Utah, called MTECH for web programming and development. For a whole
          year, I dedicated 50 hours a week learning fundamentals of web
          development from back-end tech, front-end tech, security, hacking, and
          development tools.
        </Paragraph>
        <Paragraph>
          At the end of the program, I was landed a paid internship at a herbal
          supplements company down in Lehi, Utah called Natures Sunshine. After
          working as an intern for 4 months, I was hired full-time! I worked
          there for around 2 years total building various tools and websites for
          the company, jump starting my career into Software Engineering!
        </Paragraph>
      </PageSegment>
    </>
  );
};

export default AboutPage;
